// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

// If you need to add additional functions, create a new section below using your tag of choice (Ex: SXP = Superxpdude)
// See the functions library wiki page for additional details


class SXP
{
	class mission
	{
		class abandonedVehicleLoop {postInit = 1;};
		class decoySetup {};
		class intelDrop {};
		class intelDropoff {};
		class intelPickup {};
		class intelPickupServer {};
		class intelSetup {};
		class paradrop {};
		class spawnArea {};
		class spectrumAdd {};
		class spectrumDevice {postInit = 1;};
		class spectrumRemove {};
		class setupFlagpole {};
		class setupVehicle {};
		class updateTask {};
		class vehicleDropLocal {};
		class vehicleDropServer {};
	};
};