// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class pmc_base: base
	{
		displayName = "PMC Base";

		primaryWeapon[] = {"CUP_arifle_ACR_blk_68","CUP_muzzle_snds_68SPC","CUP_acc_Flashlight","CUP_optic_CompM4",{"CUP_30Rnd_680x43_Stanag_Tracer_Red",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_ACPC2_F","muzzle_snds_acp","acc_flashlight_pistol","",{"9Rnd_45ACP_Mag",8},{},""};
		binocular = "Binocular";

		uniformClass = "CUP_U_CRYE_BLK_Full";
		headgearClass = "tmtm_h_fastEars_black";
		facewearClass = "G_AirPurifyingRespirator_01_F";
		vestClass = "tmtm_v_modularVestLite_black";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_anprc152","ItemCompass","ChemicalDetector_01_watch_F",""};

		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1},{"9Rnd_45ACP_Mag",2,8}};
		vestItems[] = {{"CUP_30Rnd_680x43_Stanag_Tracer_Red",7,30},{"MiniGrenade",2,1},{"SmokeShell",2,1}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_adenosine",2},{"ACE_epinephrine",3},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		advMedUniform[] = {{"ACE_fieldDressing",8},{"ACE_adenosine",2},{"ACE_epinephrine",3},{"ACE_morphine",1},{"ACE_tourniquet",1},{"ACE_splint",1}};
	};
	
	class B_Soldier_F: pmc_base
	{
		displayName = "PMC Base";
		
		secondaryWeapon[] = {"CUP_launch_M72A6","","","",{"CUP_M72A6_M",1},{},""};
	};
	
	class B_Soldier_SL_F: pmc_base
	{
		displayName = "Squad Leader";
		
		vestClass = "tmtm_v_modularVestGL_blackOrange";
		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ChemicalDetector_01_watch_F",""};
	};
	
	class B_Soldier_TL_F: pmc_base
	{
		displayName = "Team Leader";
		
		vestClass = "tmtm_v_modularVestGL_blackPurple";
		
		vestItems[] = {{"CUP_30Rnd_680x43_Stanag_Tracer_Red",5,30},{"MiniGrenade",2,1},{"SmokeShell",2,1},{"1Rnd_HE_Grenade_shell",10,1}};
	};
	
	class B_recon_F: pmc_base
	{
		displayName = "EW Specialist";
		
		handgunWeapon[] = {"hgun_esd_01_F","muzzle_antenna_01_f","acc_esd_01_flashlight","",{},{"ESD_01_DummyMagazine_1",812},""};
		
		uniformItems[] = {{"ACE_EntrenchingTool",1},{"ItemcTabHCam",1}};
	};
	
	class B_Medic_F: pmc_base
	{
		displayName = "Medic";
		
		vestClass = "tmtm_v_modularVestGL_blackGreen";
		backpackClass = "B_FieldPack_blk";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",20},{"ACE_adenosine",20},{"ACE_morphine",10},{"ACE_bloodIV",7}};
		advMedBackpack[] = {{"ACE_fieldDressing",20},{"ACE_elasticBandage",20},{"ACE_packingBandage",20},{"ACE_quikclot",20},{"ACE_epinephrine",15},{"ACE_adenosine",15},{"ACE_morphine",5},{"ACE_bloodIV",5},{"ACE_surgicalKit",1},{"ACE_splint",10}};
	};
	
	class cmd_medic: B_Medic_F
	{
		displayName = "Command Medic";
		
		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ChemicalDetector_01_watch_F",""};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",15},{"ACE_adenosine",10},{"ACE_morphine",5},{"ACE_bloodIV",5}};
		advMedBackpack[] = {{"ACE_fieldDressing",15},{"ACE_elasticBandage",15},{"ACE_packingBandage",15},{"ACE_quikclot",10},{"ACE_epinephrine",15},{"ACE_adenosine",10},{"ACE_morphine",5},{"ACE_bloodIV",5},{"ACE_surgicalKit",1},{"ACE_splint",5}};
	};
	
	class B_Officer_F: pmc_base
	{
		displayName = "Commander";
		
		headgearClass = "CUP_H_SLA_BeretRed";
		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ChemicalDetector_01_watch_F",""};
	};
};