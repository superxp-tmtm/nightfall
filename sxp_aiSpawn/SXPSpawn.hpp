class SXP_spawn
{
	class garrisons
	{
		class example
		{
			side = 0; // BLUFOR
			units[] = {"B_soldier_SL_F", "B_Soldier_F"}; // Array of unit classnames
			unitWeights[] = {}; // Optional. If empty, all units have equal weight
			unitCount = 30; // Default unit count for garrison list.
			buildingBlacklist[] = {}; // Building blacklist for this garrison
			buildingOccupancy = 0.75; // % of a single building that can be occupied
		};
		class looters
		{
			side = 2; // Independent
			units[] = {"I_L_Looter_Rifle_F","I_L_Looter_Pistol_F","I_L_Looter_SG_F","I_L_Looter_SMG_F"};
			unitWeights[] = {0.5,0.1,0.2,0.2};
			unitCount = 10;
			buildingBlacklist[] = {};
			buildingOccupancy = 0.75;
		};
		class insurgents
		{
			side = 2; // Independent
			units[] = {"CUP_I_GUE_Soldier_AKS74","CUP_I_GUE_Soldier_AR","CUP_I_GUE_Soldier_MG","CUP_I_GUE_Ammobearer","CUP_I_GUE_Medic"};
			unitWeights[] = {0.5,0.15,0.05,0.2,0.1};
			unitCount = 10;
			buildingBlacklist[] = {};
			buildingOccupancy = 0.75;
		};
		class military
		{
			side = 1; // OPFOR
			units[] = {"CUP_O_sla_Soldier","CUP_O_sla_Soldier_AR","CUP_O_sla_Soldier_MG","CUP_O_sla_Medic","CUP_O_sla_Spotter","CUP_O_sla_Soldier_GL"};
			unitWeights[] = {0.4,0.15,0.05,0.1,0.2,0.1};
			unitCount = 10;
			buildingBlacklist[] = {};
			buildingOccupancy = 0.75;
		};
	};
	class spawnLists
	{
		class example
		{
			// Array of groups to spawn
			// Entries should consist of the an array followed by the weight
			// Entries are an array of the group, and the "cost" of the group
			// Group definitions should be either the classname of the group, or an array of unit classnames
			groups[] = {
				// NATO infantry squad, has a "cost" of 8, and a weight of 1
				{"BUS_InfSquad",8}, 1,
				// Spawns specific units
				{{"B_soldier_SL_F", "B_soldier_F", "B_soldier_F", "B_soldier_F"}, 4}, 1
			};
			transports[] = {
				{"B_Truck_01_covered_F", 1}, 1
			};
			landVehicles[] = {
				{"B_MBT_01_cannon_F",50}, 1
			};
			airVehicles[] = {
				{"B_Plane_CAS_01_dynamicLoadout_F", 150}, 1
			};
			weights[] = {
				1, // Infantry
				1, // Land vehicles
				1 // Air vehicles
			};
		};
	};
};