// onPlayerRespawn.sqf
// Executes on a player's machine when they respawn
// _this = [<newUnit>, <oldUnit>, <respawn>, <respawnDelay>]
_this params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Call the template onPlayerRespawn function
_this call XPT_fnc_onPlayerRespawn; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Vehicle paradrop smokes
if (toLower (typeOf _newUnit) in ["b_officer_f","b_soldier_sl_f"]) then {
	private _smokeCount = 0;
	if (isNull _oldUnit) then {
		_smokeCount = 4;
	} else {
		_smokeCount = {_x == "SmokeShellOrange"} count magazines _oldUnit;
	};
	_newUnit addMagazines ["SmokeShellOrange",_smokeCount];
	[_oldUnit,"SmokeShellOrange"] remoteExec ["removeMagazines",_oldUnit]; // Remove smoke grenades from the old body
};