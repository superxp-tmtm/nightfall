// Local script for vehicle paradrops
// Called from an ace_firedPlayer CBA event handler

params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];

// Failsafe in case this fires from something else
if (_unit != player) exitWith {};

// Check if the magazine is the correct smoke grenade
if (_magazine != "SmokeShellOrange") exitWith {};

private _throwTime = cba_missionTime;
waitUntil {!(_unit getVariable ["ace_advanced_throwing_primed",false]) AND (cba_missionTime >= _throwTime + 1)}; // Make sure that we don't trigger until the grenade has actually been thrown
waitUntil {(speed _projectile == 0) OR (cba_missionTime >= _throwTime + 15)}; // Wait until the smoke grenade has stopped moving
private _smokePos = getPosATL _projectile; // Store the smoke grenade's location

[_projectile,_smokePos] remoteExec ["SXP_fnc_vehicleDropServer", 2];