// SXP_fnc_intelSetup
// Handles setting up the intel laptops (and adding them to the spectrum device array)

params ["_obj", "_id"];

if ((isNil "_obj") OR {isNull _obj}) exitWith {}; // Exit if the object has already been deleted

_obj allowDamage false;

if (isServer) then {
	_obj setVariable ["intel_id", _id, true];
	private _freq = ((floor random 110)/10) + 78; // Base frequency (78) plus a random number between 0 and 10.9
	[_obj,_freq,1000] call SXP_fnc_spectrumAdd;
};

if (hasInterface) then {
	// Create the holdAction
	[
		_obj,
		"take laptop",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		"_this distance _target < 3",
		"_caller distance _target < 3",
		{},
		{},
		{_this call SXP_fnc_intelPickup},
		{},
		[],
		3,
		1000,
		false,
		false
	] call BIS_fnc_holdActionAdd;
};