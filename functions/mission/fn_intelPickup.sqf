// Local script to handle picking up intel laptops
// Called from a holdAction
// params ["_target", "_caller", "_actionId", "_arguments"];

if !(_caller canAdd "Laptop_Closed") exitWith {
	hint "You do not have enough inventory space to pick up the laptop!";
};

private _id = _target getVariable ["intel_id",-1]; // Check the ID
if (_id == -1) exitWith {};
// Get the magazine index
private _magazinesBefore = [_caller, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;
_caller addMagazine "Laptop_Closed";
private _magazinesAfter = [_caller, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;
private _magazineID = _magazinesAfter - _magazinesBefore;
if (_magazineID isEqualTo []) exitWith {};
_magazineID = _magazineID select 0;
_this remoteExec ["SXP_fnc_intelPickupServer", 2];
// Mark down the magazine id
missionNamespace setVariable [format ["laptop_%1_id",_id],_magazineID,true];

// Update the unit's intelObjects array
_caller setVariable ["SXP_intelObjectsOld", [_caller, "Laptop_Closed"] call CBA_fnc_getMagazineIndex, true];