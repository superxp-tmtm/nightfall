// SXP_fnc_intelDrop
// Handles cases where a player dies or disconnects while holding a piece of intel

params ["_unit"];

if ((isNil "_unit") OR {isNull _unit}) exitWith {};

private _intelArr = if (_unit isKindOf "Man") then {
	[_unit, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;
} else {
	_unit getVariable ["SXP_containerIntel", []];
};

_unit removeMagazines "Laptop_Closed"; // Remove old magazines

{
	private _intelID = switch (toLower _x) do {
		case (toLower (missionNamespace getVariable ["laptop_1_id", "-1"])): {1};
		case (toLower (missionNamespace getVariable ["laptop_2_id", "-1"])): {2};
		case (toLower (missionNamespace getVariable ["laptop_3_id", "-1"])): {3};
		case (toLower (missionNamespace getVariable ["laptop_4_id", "-1"])): {4};
		case (toLower (missionNamespace getVariable ["laptop_5_id", "-1"])): {5};
		case (toLower (missionNamespace getVariable ["laptop_6_id", "-1"])): {6};
		case (toLower (missionNamespace getVariable ["laptop_7_id", "-1"])): {7};
		case (toLower (missionNamespace getVariable ["laptop_8_id", "-1"])): {8};
		default {-1};
	};
	if (_intelID != -1) then {
		private _newObj = createVehicle ["Land_Laptop_Unfolded_F",getPos _unit,[],0,"NONE"];
		[_newObj, _intelID] remoteExec ["SXP_fnc_intelSetup",0,_newObj];
	};
} forEach _intelArr;