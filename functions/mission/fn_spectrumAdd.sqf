// SXP_fnc_spectrumAdd
// Adds an object to the spectrum analyzer's array
// To be executed only on the server
if (!isServer) exitWith {};

params [
	["_obj", objNull, [objNull]],
	["_freq", -1, [0]],
	["_str", -1, [0]]
];

if (isNull _obj) exitWith {};
if (_freq == -1) exitWith {};
if (_str == -1) exitWith {};

// If the array doesn't exist, create it
if (isNil "SXP_esd_sources") then {
	SXP_esd_sources = [];
};

// Add the object to the ESD array
SXP_esd_sources append [[_obj,_freq,_str]];
publicVariable "SXP_esd_sources";