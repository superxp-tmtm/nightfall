// SXP_fnc_setupFlagpole
// Handles setting up the paradrop flagpole at spawn

params ["_obj"];

if ((isNil "_obj") OR {isNull _obj}) exitWith {};

_obj allowDamage false;

if (hasInterface) then {
	// Create the holdAction
	[
		_obj,
		"paradrop on squad",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",
		"_this distance2D _target < 5",
		"_caller distance2D _target < 5",
		{},
		{},
		{_this call SXP_fnc_paradrop},
		{},
		[],
		5,
		1000,
		false,
		false
	] call BIS_fnc_holdActionAdd;
};