// Local script to handle dropping off intel laptops
// Called from a holdAction
// params ["_target", "_caller", "_actionId", "_arguments"];

if (!local _caller) exitWith {}; // Failsafe to only run on the client machine

// Check if the player has any intel laptops on them
private _laptopCount = {_x == "Laptop_Closed"} count magazines _caller;
if (_laptopCount <= 0) exitWith {systemChat "You do not have any intel to turn in."};

// Check which laptops the player has on them
private _laptops = [_caller, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;

{
	switch (_x) do {
		case (missionNamespace getVariable ["laptop_1_id","0"]): {["intel_1_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_2_id","0"]): {["intel_2_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_3_id","0"]): {["intel_3_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_4_id","0"]): {["intel_4_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_5_id","0"]): {["intel_5_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_6_id","0"]): {["intel_6_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_7_id","0"]): {["intel_7_drop"] remoteExec ["SXP_fnc_updateTask",2];};
		case (missionNamespace getVariable ["laptop_8_id","0"]): {["intel_8_drop"] remoteExec ["SXP_fnc_updateTask",2];};
	};
} forEach _laptops;

// Remove all laptops in inventory
for "_i" from 1 to _laptopCount do {
	_caller removeMagazineGlobal "Laptop_Closed";
};

// Update the carried intel array
_caller setVariable ["SXP_intelObjectsOld", [_caller, "Laptop_Closed"] call CBA_fnc_getMagazineIndex, true];