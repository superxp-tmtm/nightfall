// Server portion of intelPickup.sqf
if (!isServer) exitWith {};

params ["_target", "_caller", "_actionId", "_arguments"];

// Remove the laptop from the spectrum device array
[_target] call SXP_fnc_spectrumRemove;

// Delete the intel object
deleteVehicle _target;