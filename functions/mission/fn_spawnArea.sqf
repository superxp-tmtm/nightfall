/*
	SXP_fnc_spawnArea
	Author: Superxpdude
	Spawns a number of enemy units within an area
	
	Executes only on the server
	
	Parameters:
		0: Trigger - Trigger that defines the spawn area
		1: Scalar - Number of enemies to spawn
		2: String - Spawn group to use
	
	Returns: Nothing
*/
// Only run on the server

if (!isServer) exitWith {};

//systemChat "Starting spawn loop";

// Define our parameters
params [
	["_trigger", nil, [objNull]],
	["_amount", 20, [0]],
	["_spawnGroup", "", [""]],
	["_patrolRange", 400, [0]]
];

(triggerArea _trigger) params [
	["_triggerX", 0],
	["_triggerY", 0],
	["_triggerAngle", 0],
	["_triggerRectangle", false]
];

private _squadLeads = [];
private _squadLeadWeights = [];
private _unitList = [];
private _unitWeights = [];
private _groupSide = EAST;

switch (toLower _spawnGroup) do {
	case "looters": {
		_squadLeads = ["I_L_Criminal_SG_F","I_L_Criminal_SMG_F"];
		_squadLeadWeights = [0.7,0.3];
		_unitList = ["I_L_Looter_Rifle_F","I_L_Looter_Pistol_F","I_L_Looter_SG_F","I_L_Looter_SMG_F"];
		_unitWeights = [0.5,0.1,0.2,0.2];
		_groupSide = Independent;
	};
	case "insurgents": {
		_squadLeads = ["CUP_I_GUE_Officer"];
		_squadLeadWeights = [1];
		_unitList = ["CUP_I_GUE_Soldier_AKS74","CUP_I_GUE_Soldier_AR","CUP_I_GUE_Soldier_MG","CUP_I_GUE_Ammobearer","CUP_I_GUE_Medic"];
		_unitWeights = [0.5,0.15,0.05,0.2,0.1];
		_groupSide = Independent;
	};
	case "military": {
		_squadLeads = ["CUP_O_sla_Soldier_SL","CUP_O_sla_Officer"];
		_squadLeadWeights = [0.95,0.05];
		_unitList = ["CUP_O_sla_Soldier","CUP_O_sla_Soldier_AR","CUP_O_sla_Soldier_MG","CUP_O_sla_Medic","CUP_O_sla_Spotter","CUP_O_sla_Soldier_GL"];
		_unitWeights = [0.4,0.15,0.05,0.1,0.2,0.1];
		_groupSide = EAST;
	};
	default {};
};

_triggerPos = getPos _trigger;
_activeDist = (sqrt ((_triggerX^2)+(_triggerY^2))) + 500; // Accounts for the corners of the square being further away from the trigger
_minDist = 400; // Prevents spawns within x distance meters of a player

private ["_groups", "_units"];
_groups = [];
_units = [];

// Wait until there are players in range

waitUntil {
	sleep 15; // Only check this every 15 seconds
	({(_triggerPos distance _x) < _activeDist} count allPlayers > 0)
};

while {_amount > 0} do {
	// Start spawning
	private ["_pos"];

	// Keep attempting to find a location until we get a good one
	_relocate = true;
	while {_relocate} do {		
		//_pos = [(_triggerPos select 0) + ((random (_triggerX * 2)) - _triggerX), (_triggerPos select 1) + ((random (_triggerY * 2)) - _triggerY), 0];
		_pos = _trigger call BIS_fnc_randomPosTrigger;
		// Check to make sure that our position is inside the trigger, on land, and away from any players
		if ((_pos inArea _trigger) and (!surfaceIsWater _pos) and (({(_pos distance [getPos (vehicle _x) select 0, getPos (vehicle _x) select 1, 0]) < _minDist} count allPlayers) == 0)) then {_relocate = false};
	};
	
	// Determine how many to spawn
	private _minCount = (3 min _amount) max 2;
	private _medCount = (5 min _amount) max 2;
	private _maxCount = (7 min _amount) max 2;
	_spawnCount = (floor (random [_minCount,_medCount,_maxCount]));
	
	_group = createGroup [_groupSide,true];
	_groups pushBackUnique _group;
	
	for "_i" from 1 to _spawnCount do {
		private ["_z"];
		if (_i == 1) then {
			_z = _group createUnit [_squadLeads selectRandomWeighted _squadLeadWeights, _pos, [], 5, "NONE"];
		} else {
			_z = _group createUnit [_unitList selectRandomWeighted _unitWeights, _pos, [], 5, "NONE"];
		};
		[_z] joinSilent _group;
		_units pushBackUnique _z;
		
		// Force add flashlights to patrols.
		// Code heavily inspired by ZEN: https://github.com/zen-mod/ZEN/blob/master/addons/modules/functions/fnc_moduleToggleFlashlights.sqf
		private _weapon = currentWeapon _z;
		private _pointer = (_z weaponAccessories _weapon) select 1;
		
		// Add a flashlight if the unit does not already have one
		if ((_weapon != "") AND {_pointer == "" || {getNumber (configFile >> "CfgWeapons" >> _pointer >> "ItemInfo" >> "FlashLight" >> "size") <= 0}}) then {
			// Get all compatible flashlights for the weapon
			private _flashlights = [_weapon,"pointer"] call CBA_fnc_compatibleItems select {
				getNumber (configFile >> "CfgWeapons" >> _x >> "ItemInfo" >> "FlashLight" >> "size") > 0
			};
			
			// Only run if we actually found compatible flashlights
			if !(_flashlights isEqualTo []) then {
				 ["ZEN_common_addWeaponItem", [_z, _weapon, selectRandom _flashlights], _z] call CBA_fnc_targetEvent;
			};
		};
		
		// Force gun lights on
		["ZEN_common_enableGunLights", [_z, "ForceOn"], _z] call CBA_fnc_targetEvent;
	};
	
	[_group, _pos, _patrolRange] call BIS_fnc_taskPatrol;
	
	// Move the units over to the HC if present
	if (missionNamespace getVariable ["XPT_headless_connected", false]) then {
		_group setGroupOwner XPT_headless_clientID;
	};
	
	/*
	// TEMP - Create a marker as a debug measure to make sure it works
	_marker = createMarkerLocal [format ["marker%1", random 1000000], _pos];
	_marker setMarkerTypeLocal "hd_dot";
	_marker setMarkerSizeLocal [0.5,0.5];
	
	switch _spawnCount do {
		case 2: {_marker setMarkerColorLocal "ColorBlue"; _marker setMarkerTextLocal "2";};
		case 3: {_marker setMarkerColorLocal "ColorGreen"; _marker setMarkerTextLocal "3";};
		case 4: {_marker setMarkerColorLocal "ColorYellow"; _marker setMarkerTextLocal "4";};
		case 5: {_marker setMarkerColorLocal "ColorOrange"; _marker setMarkerTextLocal "5";};
		case 6: {_marker setMarkerColorLocal "ColorRed"; _marker setMarkerTextLocal "6";};
		default {_marker setMarkerColorLocal "ColorRed"; _marker setMarkerTextLocal format ["%1",_spawnCount];};
	};
	*/
	
	_amount = _amount - _spawnCount;
	
};

// Add all units spawned to all curators
{
	_x addCuratorEditableObjects [_units, true];
} forEach allCurators;

// Wait until all players have left the area
waitUntil {
	sleep 60; // Only check this every 60 seconds
	({(_triggerPos distance _x) < (_activeDist) * 2} count allPlayers <= 0)
};

// Start unit cleanup
{
	if (alive _x) then {
		_x setDamage 1;
		_amount = _amount + 1;
	};
	hideBody _x;
} forEach _units;

// If we have any available units left, restart the script.
if (_amount > 0) then {
	[_trigger, _amount, _spawnGroup] spawn SXP_fnc_spawnArea;
};