// Runs the spectrum device on each frame
// Array of arrays for each spectrum signal
// Each subarray must contain an object, frequency, signal distance (in meters).
if (isServer) then {
	if (isNil "SXP_esd_sources") then {
		SXP_esd_sources = [];
		publicVariable "SXP_esd_sources";
	};
};

if (!hasInterface) exitWith {};

// Frequency range for military antenna
missionNamespace setVariable ["#EM_FMin", 78];
missionNamespace setVariable ["#EM_FMax", 89];

// Sensitivity
missionNamespace setVariable ["#EM_SMin", -60];
missionNamespace setVariable ["#EM_SMax", -10];

// Selected frequency
missionNamespace setVariable ["#EM_SelMin", 81.0];
missionNamespace setVariable ["#EM_SelMax", 81.5];

[] spawn {
	waitUntil {!isNil "SXP_esd_sources"};
	addMissionEventHandler ["EachFrame", {
		// Define some variables
		private _minDirDiff = 5; // Minimum direction difference before signal begins to drop
		private _maxDirDiff = 60; // Maximum direction difference before signal is at minimum strength
		
		private _maxSignalStr = 50; // Maximum signal strength above 0
		private _signalStrMod = -60; // Modifier applied to signal strength result
		// Only run if the player has the spectrum device selected
		if ("hgun_esd_" in (currentWeapon player)) then {
			private _emSignals = [];
			private _maxSignal = 0;
			private _em_selMin = missionNamespace getVariable ["#EM_SelMin", 0];
			private _em_selMax = missionNamespace getVariable ["#EM_SelMax", 0];
			
			{
				private _obj = _x select 0;
				private _freq = _x select 1;
				private _str = _x select 2;
				private ["_effectiveStr", "_effectiveStrDist", "_effectiveStrDir", "_relDir"];
				
				private _dist = player distance _obj;
				// Only continue if the player is within signal distance
				if (_dist <= _str) then {
					_relDir = player getRelDir _obj;
					_effectiveStrDist = linearConversion [_str,0,_dist,0,1,true]; // Str for distance
					_effectiveStrDir = (linearConversion [_maxDirDiff,_minDirDiff,_relDir,0.2,1,true]) max (linearConversion [360 - _maxDirDiff,360 - _minDirDiff,_relDir,0.2,1,true]); // Str for direction
					_effectiveStr = ((_maxSignalStr * _effectiveStrDist) * _effectiveStrDir) + _signalStrMod; // Calculate the strength
					_emSignals append [_freq, _effectiveStr];
					if ((_freq >= _em_selMin) AND (_freq <= _em_selMax)) then {
						_maxSignal = _maxSignal max (_effectiveStrDist * _effectiveStrDir);
					};
				};
			} forEach SXP_esd_sources;
			// Set the signals array
			missionNamespace setVariable ["#EM_Values",_emSignals];
			// Set the "scanning progress" (we're using this to make it easier to track signals
			missionNamespace setVariable ["#EM_Progress", _maxSignal];
		};
	}];
};