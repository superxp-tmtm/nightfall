// SXP_fnc_decoySetup
// Handles setting up a spectrum device decoy
if (!isServer) exitWith {};

params [
	["_obj", objNull, [objNull]],
	["_freq", (((floor random 110)/10) + 78), [0]],
	["_str", 500, [0]]
];

//systemChat format ["[%1] [%2] [%3]", _obj, _freq, _str];

if (isNull _obj) exitWith {};

// Add the object to the ESD array
[_obj,_freq,_str] call SXP_fnc_spectrumAdd;


// Add the event handler to remove the object if destroyed
_obj addMPEventHandler ["MPKilled", {
	params ["_unit", "_killer", "_instigator", "_useEffects"];
	if (isServer) then {
		[_unit] call SXP_fnc_spectrumRemove;
	};
}];