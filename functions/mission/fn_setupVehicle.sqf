// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// Airdropped prowlers
	case (toLower "B_LSV_01_unarmed_black_F"):
	{
		[_newVeh, "prowler_drop"] call XPT_fnc_loadItemCargo;
	};
	// Pre-placed prowlers
	case (toLower "B_LSV_01_unarmed_F"): {
		// Configure the appearance
		[
			_newVeh,
			["Black",1], 
			["HideDoor1",0,"HideDoor2",0,"HideDoor3",0,"HideDoor4",0]
		] call BIS_fnc_initVehicle;
		
		[_newVeh, "prowler"] call XPT_fnc_loadItemCargo;
	};
};