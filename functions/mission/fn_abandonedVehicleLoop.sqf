/*
	SXP_fnc_abandonedVehicleLoop
	Author: Superxpdude
	Checks for "abandoned" player vehicles outside of the starting area.
	
	Executes only on the server
	
	Parameters:
		None
	
	Returns: Nothing
*/
// Only run on the server

if (!isServer) exitWith {};

[] spawn {
	waitUntil {cba_missionTime >= 300};
	while {true} do {
		{
			private _veh = _x;
			// Check if any players are nearby, and if the vehicle is in the spawn zone
			private _occupied = count (crew _veh) != 0;
			private _vehicleSafe = _veh inArea spawn_area_trigger;
			private _lastRaidTime = _veh getVariable ["sxp_veh_last_raid_time",0];
			if ((!_occupied AND !_vehicleSafe AND (cba_missionTime - _lastRaidTime > 300)) AND {count (allPlayers select {(_veh distance _x) <= 1000}) == 0}) then {
				// No players are nearby
				_veh setVariable ["sxp_veh_last_raid_time", cba_missionTime, true];
				_veh setDamage 0.4;
				[_veh,0.6] remoteExec ["setFuel", _veh];
				// Spawn a group of insurgents near the vehicle
				private _pos = [_veh, 150, 250] call BIS_fnc_findSafePos;
				private _group = createGroup [independent,true];
				{
					private _unit = _group createUnit [_x,_pos,[],5,"NONE"];
				} forEach ["CUP_I_GUE_Officer","CUP_I_GUE_Soldier_AR","CUP_I_GUE_Soldier_MG","CUP_I_GUE_Soldier_AKS74","CUP_I_GUE_Soldier_AKS74","CUP_I_GUE_Medic"];
				// Add spawned units to zeus
				{
					_x addCuratorEditableObjects [units _group, true];
				} forEach allCurators;
				private _wp1 = _group addWaypoint [_veh,-1];
				_wp1 setWaypointType "GETIN";
				_wp1 waypointAttachVehicle _veh;
				
				// Grab the closest player that is not within the spawn area.
				private _targetDistance = 1e10;
				private ["_targetPos"];
				{
					private _dist = _x distance _veh;
					private _inSpawn = _x inArea spawn_area_trigger;
					if (!_inSpawn AND {_dist < _targetDistance}) then {
						_targetDistance = _dist;
						_targetPos = _x;
					};
				} forEach allPlayers;
				
				private _wp2 = _group addWaypoint [_targetPos,-1];
				_wp2 setWaypointType "SAD";
				_wp2 waypointAttachVehicle _targetPos;
				
				// Make sure they end up finding someone
				private _wp3 = _group addWaypoint [_targetPos,-1];
				_wp3 setWaypointType "DESTROY";
				_wp3 waypointAttachVehicle _targetPos;
			};
		} forEach entities [["B_LSV_01_unarmed_F","B_LSV_01_unarmed_black_F"],[],false,true];
		sleep 10;
	};
};