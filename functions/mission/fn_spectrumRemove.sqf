// SXP_fnc_spectrumRemove
// Removes an object from the spectrum analyzer's array
// To be executed only on the server
if (!isServer) exitWith {};

params [
	["_obj", objNull, [objNull]]
];

if (isNull _obj) exitWith {};

// If the array doesn't exist, create it
if (isNil "SXP_esd_sources") then {
	SXP_esd_sources = [];
};

SXP_esd_sources deleteAt (SXP_esd_sources findIf {_x select 0 == _obj});
publicVariable "SXP_esd_sources";