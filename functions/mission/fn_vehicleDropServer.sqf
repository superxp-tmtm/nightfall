// Server script for vehicle paradrops
// Called from SXP_fnc_vehicleDropLocal

params ["_projectile", "_smokePos"];

_smokePos set [2,500]; // Set the position to 500m in the air
// Create the vehicle
private _veh = createVehicle ["B_LSV_01_unarmed_black_F",_smokePos,[],0,"NONE"];
[_veh] call SXP_fnc_setupVehicle;
// Add the vehicle to all curators
{
	_x addCuratorEditableObjects [[_veh],true];
} forEach allCurators;

// Wait until the vehicle is low enough to open the parachute
waitUntil {(getPos _veh) select 2 < 170};
private _chute = createVehicle ["B_Parachute_02_F", _veh modelToWorld [0,0,0],[],0,"CAN_COLLIDE"];
_chute setVelocity (velocity _veh);
_chute setDir (getDir _veh);
_veh attachTo [_chute,[0,0,0]];

// When the vehicle is low enough, detach the parachute
waitUntil {(getPos _veh) select 2 < 4};
detach _veh;
_chute setVelocity [5,0,0];