// Function for updating mission tasks when objectives are completed.
if (!isServer) exitWith {};
switch (toLower (_this select 0)) do {
	case "intel_1_drop": {
		["intel_1","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_2_drop": {
		["intel_2","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_3_drop": {
		["intel_3","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_4_drop": {
		["intel_4","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_5_drop": {
		["intel_5","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_6_drop": {
		["intel_6","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_7_drop": {
		["intel_7","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "intel_8_drop": {
		["intel_8","SUCCEEDED"] call BIS_fnc_taskSetState;
		["mission_complete"] spawn SXP_fnc_updateTask;
	};
	case "mission_complete": {
		if (({!(_x call BIS_fnc_taskCompleted)} count ["intel_1","intel_2","intel_3","intel_4","intel_5","intel_6","intel_7","intel_8"]) <= 0) then {
			[] spawn {
				sleep 5;
				["victory",true,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
			};
		};
	};
};