// SXP_fnc_paradrop
// Paradrops a player unit on their squad leader
// Called from a holdaction
params ["_target", "_caller", "_actionId", "_arguments"];
private ["_dropPos"];

if !(local _caller) exitWith {};

if ((count units group _caller) < 2) exitWith {
	systemChat "You can't paradrop without your squadmates!";
};

if (_caller == leader group _caller) then {
	// If the player is the squad leader, drop them on a random squadmate
	_dropPos = getPosATL (selectRandom ((units group _caller) - [_caller]));
} else {
	_dropPos = getPosATL (leader group _caller);
};
_dropPos set [2,1000];

if ((backpack _caller) != "") then {
	[_caller] call zade_boc_fnc_actionOnChest; // Move the player's backpack to their chest
};

_caller addBackpack "B_Parachute"; // Give the player a parachute

_caller setPosATL _dropPos;