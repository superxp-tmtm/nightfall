// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Uncomment the line below to use briefing.sqf for mission briefings. Un-needed if you're using XPTBriefings.hpp
[] execVM "scripts\briefing.sqf";

// Loadout check
//if !(1021790 in getDLCs 1) then {
//	_player setVariable ["loadout", (_player getVariable ["loadout",typeOf _player]) + "_nodlc"];
//};

// Display the mission start text
[] spawn {
	sleep 5;
	[parseText format ["<t align='right' size='1.6'><t font='PuristaBold' size='1.8'>%1<br/></t>%2<br/>%3</t>",
		toUpper "Nightfall", 
		"by Superxpdude", 
		"20:00:00"],
		true,
		nil,
		10
	] call BIS_fnc_textTiles;
};

// Vehicle paradrop event handler
["ace_firedPlayer", {
	_this spawn SXP_fnc_vehicleDropLocal;
}] call CBA_fnc_addEventHandler;

// Intel drop holdaction
[
	intelDropBox,
	"turn in intel",
	"\a3\ui_f\data\igui\cfg\holdactions\holdaction_loaddevice_ca.paa",
	"\a3\ui_f\data\igui\cfg\holdactions\holdaction_loaddevice_ca.paa",
	"((player distance _target) < 5)",
	"((player distance _target) < 5)",
	nil,
	nil,
	{_this call SXP_fnc_intelDropoff},
	nil,
	[],
	3,
	1000, // Maximum priority
	false,
	false
] call BIS_fnc_holdActionAdd;

// Intel inventory event handlers
_player addEventHandler ["Put", {
	params ["_unit", "_container", "_item"];
	//systemChat format ["Put [%1] at [%2]", _this select 2, cba_missionTime];
	private _currentIntel = [_unit, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;
	private _oldIntel = _unit getVariable ["SXP_intelObjectsOld", []];
	{
		private _id = _x;
		_oldIntel deleteAt (_oldIntel findIf {_x == _id});
	} forEach _currentIntel;
	//systemChat format ["Dropped intel: %1", _oldIntel];
	private _containerIntel = _container getVariable ["SXP_containerIntel", []];
	_containerIntel pushBackUnique (_oldIntel select 0); // Add the magazine ID to the container
	_container setVariable ["SXP_containerIntel", _containerIntel, true];
	_unit setVariable ["SXP_intelObjectsOld", [_unit, "Laptop_Closed"] call CBA_fnc_getMagazineIndex, true];
}];
_player addEventHandler ["Take", {
	params ["_unit", "_container", "_item"];
	//systemChat format ["Take [%1] at [%2]", _this select 2, cba_missionTime];
	private _currentIntel = [_unit, "Laptop_Closed"] call CBA_fnc_getMagazineIndex;
	private _containerIntel = _container getVariable ["SXP_containerIntel", []];
	{
		private _id = _x;
		_containerIntel deleteAt (_containerIntel findIf {_x == _id});
	} forEach _currentIntel;
	_container setVariable ["SXP_containerIntel", _containerIntel, true];
	_unit setVariable ["SXP_intelObjectsOld", [_unit, "Laptop_Closed"] call CBA_fnc_getMagazineIndex, true];
}];