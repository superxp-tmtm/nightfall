// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

player createDiaryRecord ["Diary", ["Reinforcements", "Because of the low visibility (and the capabilities of our aircraft), we have access to airborne resupplies during the operation.
<br/><br/>Any JIP or Respawning players can use the flagpole at the start location to paradrop onto their squad leader. Keep in mind that this will leave you vulnerable as you descend.
<br/><br/>Squad Leaders (and the commander) are equipped with four non-replenishing Orange Smoke Grenades. Throwing one of these grenades will signal our aircraft to drop a replacement Prowler on your location.
The replacement vehicle will contain an explosive charge in its cargo. You are to use this charge to ensure that vehicles that we need to abandon cannot be used against us.
Keep in mind that these smoke grenades are not replenished on respawn. You will be provided with exactly as many grenades as there are left on your corpse."]];

player createDiaryRecord ["Diary", ["Spectrum Device", "The Electronic Warface Specialists have been equipped with Spectrum Devices, capable of locating the laptops that we're looking for.
<br/>By right-clicking, you can view a 'spectrum graph' of any signals that your Spectrum Device is picking up. Signals will get stronger as you turn to face them, and as you approach them.
<br/>Keep in mind, these laptops are not the only things that can be emitting eletro-magnetic interference. You may have to account for other signal sources as well."]];

player createDiaryRecord ["Diary", ["CBRN", "This mission contains chemically contaminated areas that would normally be unsafe for humans.
Thankfully, you are equipped with Personal Protective Equipment to keep yourself safe from these chemical threats. Please keep note of the following points:
<br/>- All players are equipped with a Chemical Detector. You can display yours by pressing your 'Watch' key (usually 'O'). You can toggle the noises it makes by double-clicking on it in your inventory.
<br/>- Most players are equipped with an APR and respirator backpack. If you have these, you must attach the respirator hose (using ACE self-interact, 'toggle respirator connection' option) in order to reach maximum protection.
<br/>- You must keep the backpack that you spawn with in order to achieve maximum protection against CBRN threats. Losing your backpack will significantly reduce the effectiveness of your CBRN gear.
<br/>- Your CBRN equipment will provide complete protection against any threat level of 3.0 or lower. Above this point, your CBRN gear will only reduce the effects of the chemical hazards."]];

player createDiaryRecord ["Diary", ["Assets", "Your assets for this mission are as follows:<br/>
	- 17x Prowler (Unarmed)<br/>
	- 4x Airdropped Prowlers (per squad)
"]];

player createDiaryRecord ["Diary", ["Intel",
"While we don't have the exact locations of the laptops that we need to recover, we have been told that they are equipped with locator beacons. Our Electronic Warfare specialists should be able to use their Spectrum Devices to pinpoint the locations of the laptops once we get close."
]];

player createDiaryRecord ["Diary", ["Mission",
"Mr. Johnson's client needs eight laptops recovered from the contaminated zone. From the little information that we were given, these laptops were in the posession of some 'employees' who were working in the area.
<br/>You have been instructed to recover the laptops at all costs, and return them to your start location, depositing them in a case in the temporary command centre in the yellow building.
<br/>Once you have recovered all eight laptops. Mr. Johnson's client will arrive to retrieve them from you."
]];

player createDiaryRecord ["Diary", ["Situation",
"Three days ago, a massive chemical incident occured near the town of Kirkonkyla. The exact cause of the incident is still unknown, but a large portion of the area has already been contaminated.
<br/>Weather has not been favourable for rescue and cleanup operations, with intense storms occuring every night since the incident. The weather has let up a bit today, but the storm is expected to return in force tonight.
<br/><br/>Yesterday you were contacted by a 'Mr. S. Johnson', who sent over the details of your operation today."
]];
