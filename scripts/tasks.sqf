// Script for creating player tasks
// Only to be run on the server. BIS_fnc_taskCreate is global.
if (!isServer) exitWith {};

// Example task syntax below
/*
[
	true, // Owners of the task. See wiki page for details
	["task name", "parent task name"], // Name of the task, along with parent name. Parent name is used for nested tasks
	["description", "title", "marker"], // Information about the task. Honestly don't know what the marker does. Leave it blank. Can also use a CfgTaskDescriptions class (class name in string form) instead of this array.
	[0,0,0], // Task destination, can also refer to object location. Good method to use is getMarkerPos. Use objNull for task without location.
	"CREATED", // Task state. Current state of task at the time it's created (usually either "CREATED" or "ASSIGNED")
	10, // Task priority. Taken into account when automatically assigning new tasks when previous tasks are completed.
	true, // Show notification. Leave this as true. Set to false to disable task popup
	"attack", // Task type. Types can be found in CfgTaskTypes, or at https://community.bistudio.com/wiki/Arma_3_Tasks_Overhaul#Appendix
	true // Share task. If true, game will report which players have the task selected.	
] call BIS_fnc_taskCreate;

	Make sure to add the name of the zeus unit into the owner field in string format
	It should look like this when written
	
		[true, "zeus_unit"]
		
	This adds the task to all player units, as well as the "zeus_unit" curator.
	This makes sure that zeus units have the same tasks that the players do
*/
// Place tasks here

[
	true,
	"intel_1",
	["The first laptop is located somewhere near Vilkkila.", "Recover Intel 1", ""],
	intel_1_pos,
	"ASSIGNED",
	10,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_2",
	["The second laptop is located somewhere near Kirkonkyla.", "Recover Intel 2", ""],
	intel_2_pos,
	"CREATED",
	8,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_3",
	["The third laptop is located somewhere near Kirkonkyla.", "Recover Intel 3", ""],
	intel_3_pos,
	"CREATED",
	7,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_4",
	["The fourth laptop is located somewhere near Eerikkala.", "Recover Intel 4", ""],
	intel_4_pos,
	"CREATED",
	6,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_5",
	["The fifth laptop is located somewhere near Hurppu.", "Recover Intel 5", ""],
	intel_5_pos,
	"CREATED",
	5,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_6",
	["The sixth laptop is located somewhere around the port east of Kirkonkyla.", "Recover Intel 6", ""],
	intel_6_pos,
	"CREATED",
	4,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_7",
	["The seventh laptop is located somewhere near Hanski.", "Recover Intel 7", ""],
	intel_7_pos,
	"CREATED",
	3,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;

[
	true,
	"intel_8",
	["The eighth laptop is located somewhere near Rannanen.", "Recover Intel 8", ""],
	intel_8_pos,
	"CREATED",
	2,
	true,
	"attack",
	true
] call BIS_fnc_taskCreate;